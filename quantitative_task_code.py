import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
plt.style.use('ggplot')

#%% Initialise
# Set path
path_list = [r'\\nsq025vs\u9\acvn737\Desktop\truck-quality',
             r'C:\Users\vu86683\Desktop\truck-quality']

for path in path_list:
    try:
        os.chdir(path)
    except FileNotFoundError:
        continue
        
# Create folder for visuals
if not os.path.isdir(r'./plots'):
    os.mkdir(r'./plots')

def saveplot(figname):
    plt.savefig(r'./plots/' + figname + '.png')

# Read data
def load_data():
    return pd.read_csv(r'./Data/VehicleData_csv.csv')

data = load_data()

#%% Initial EDA
# Overview
data.describe()
data.info()

# View first 10 rows
print(data.head(10))

# Check for missing values
data.isnull().any() # No missing values

# Summarise by Vehicle Type and Manufacturer
## One feature at a time
for col in ['VehicleType', 'Manufacturer']:
    counts = data[col].value_counts()
    plt.figure(figsize = (12,7))
    plt.bar(counts.index, counts)
    plt.xlabel(col)
    plt.ylabel('Count')
    plt.title('Count by {}'.format(col))
    plt.tight_layout()
    plt.show()
    saveplot('count_by'.format(col))

## 2 features at the same time
plt.figure(figsize = (12,7))
data.groupby(['VehicleType', 'Manufacturer'])['VehicleID']\
            .count()\
            .sort_values(ascending = True)\
            .plot(kind = 'barh')
plt.xlabel('Count')
plt.title('Vehicle Count by Manufacturer and Type')
plt.tight_layout()
saveplot('Count by Vehicle Type and Manufacturer')

# Distribution of condition score
plt.figure()
plt.hist(data['ConditionScore'], bins = 10)

#%% Question 1 - How does sample size vary from year-to-year
# Count number of sample by year
sample_by_year = data.groupby('FinancialYear').count()['VehicleID']

# Visualise
fig = plt.figure()
plt.bar(sample_by_year.index, sample_by_year)
plt.xticks(rotation = 45)
plt.xlabel('Financial Year')
plt.ylabel('Sample counts')
plt.title('Number of sample by financial year')
plt.tight_layout()
plt.show()
saveplot('sample_by_fy')

#%% Question 1 - Exclude any year?
data2001 = data[data.FinancialYear == '2000-01']
data2001['VehicleType'].value_counts()
data2001['Manufacturer'].value_counts()

#%% Question 2 - Condition of vehicles change over time?
# Average score by year
score_by_year = data.groupby('FinancialYear')['ConditionScore'].mean()

# Visualise
fig = plt.figure()
plt.plot(score_by_year, label = 'average condition score')
plt.plot([score_by_year.mean()]*len(score_by_year), label = 'all years average')
plt.xticks(rotation=45)
plt.xlabel('Financial Year')
plt.ylabel('Average condition score')
plt.legend()
plt.title('Average condition score by financial year')
plt.tight_layout()
plt.show()
saveplot('score_by_fy')

#%% Question 3 - Vehice type needs attention
def condition_by_factor(factorlist):
    df = pd.pivot_table(data, 
                        values = 'ConditionScore',
                        index = 'FinancialYear',
                        columns = factorlist,
                        aggfunc = 'mean')
                        
    plt.figure(figsize = (12,7))
    for col in df.columns:
        plt.plot(df[col], label = col)
        
    plt.xticks(rotation = 45)
    plt.xlabel('Financial Year')
    plt.ylabel('Average condition score')
    plt.legend()
    plt.title('Condition score by {}'.format(factorlist))
    plt.tight_layout()
    plt.show()
    
    saveplot('condition_by_{}'.format(factorlist))
    
    return df

condition_by_factor(['VehicleType'])
condition_by_factor(['Manufacturer'])
#condition_by_factor(['VehicleType', 'Manufacturer'])


#%% Question 5 - Calculating sample size
error_margin = 1
zscore = 1.96
stdev = data.ConditionScore.std()

samplesize = (zscore * stdev / error_margin)**2